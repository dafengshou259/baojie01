export default {
  getType(instance) {
    return Object.prototype.toString.call(instance).slice(8, -1);
  }
};
